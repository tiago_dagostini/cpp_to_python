import numpy as np
import active_cpp
from scipy import misc
import imageio
import glob
images ={}
import os
import imageio
from skimage.color import rgb2gray
from skimage import data
from skimage.filters import gaussian
#from skimage.segmentation import active_contour
#import active_contour
from warnings import warn
import numpy as np
from scipy.interpolate import RectBivariateSpline
from skimage.util import img_as_float
from skimage.filters import sobel
#from ..util import img_as_float
#from ..filters import sobel


def active_contour(image, snake, alpha=0.01, beta=0.1,
                   w_line=0, w_edge=1, gamma=0.01,
                   bc=None, max_px_move=1.0,
                   max_iterations=2500, convergence=0.1,
                   *,
                   boundary_condition='periodic',
                   coordinates=None):
    
    if bc is not None:       
        boundary_condition = bc
    if coordinates is None:       
        coordinates = 'xy'
        snake_xy = snake
    if coordinates == 'rc':
        snake_xy = snake[:, ::-1]
    max_iterations = int(max_iterations)
    if max_iterations <= 0:
        raise ValueError("max_iterations should be >0.")
    convergence_order = 10
    valid_bcs = ['periodic', 'free', 'fixed', 'free-fixed',
                 'fixed-free', 'fixed-fixed', 'free-free']
    if boundary_condition not in valid_bcs:
        raise ValueError("Invalid boundary condition.\n" +
                         "Should be one of: "+", ".join(valid_bcs)+'.')
    img = img_as_float(image)
    RGB = img.ndim == 3

    # Find edges using sobel:
    if w_edge != 0:
        if RGB:
            edge = [sobel(img[:, :, 0]), sobel(img[:, :, 1]),
                    sobel(img[:, :, 2])]
        else:
            edge = [sobel(img)]
        for i in range(3 if RGB else 1):
            edge[i][0, :] = edge[i][1, :]
            edge[i][-1, :] = edge[i][-2, :]
            edge[i][:, 0] = edge[i][:, 1]
            edge[i][:, -1] = edge[i][:, -2]
    else:
        edge = [0]

    # Superimpose intensity and edge images:
    if RGB:
        img = w_line*np.sum(img, axis=2) \
            + w_edge*sum(edge)
    else:
        img = w_line*img + w_edge*edge[0]

    # Interpolate for smoothness:
    intp = RectBivariateSpline(np.arange(img.shape[1]),
                               np.arange(img.shape[0]),
                               img.T, kx=2, ky=2, s=0)

    x, y = snake_xy[:, 0].astype(np.float), snake_xy[:, 1].astype(np.float)
    n = len(x)
    xsave = np.empty((convergence_order, n))
    ysave = np.empty((convergence_order, n))

    # Build snake shape matrix for Euler equation
    a = np.roll(np.eye(n), -1, axis=0) + \
        np.roll(np.eye(n), -1, axis=1) - \
        2*np.eye(n)  # second order derivative, central difference
    b = np.roll(np.eye(n), -2, axis=0) + \
        np.roll(np.eye(n), -2, axis=1) - \
        4*np.roll(np.eye(n), -1, axis=0) - \
        4*np.roll(np.eye(n), -1, axis=1) + \
        6*np.eye(n)  # fourth order derivative, central difference
    A = -alpha*a + beta*b
   
    #print(A)
    # Impose boundary conditions different from periodic:
    sfixed = False
    if boundary_condition.startswith('fixed'):
        A[0, :] = 0
        A[1, :] = 0
        A[1, :3] = [1, -2, 1]
        sfixed = True
    efixed = False
    if boundary_condition.endswith('fixed'):
        A[-1, :] = 0
        A[-2, :] = 0
        A[-2, -3:] = [1, -2, 1]
        efixed = True
    sfree = False
    if boundary_condition.startswith('free'):
        A[0, :] = 0
        A[0, :3] = [1, -2, 1]
        A[1, :] = 0
        A[1, :4] = [-1, 3, -3, 1]
        sfree = True
    efree = False
    if boundary_condition.endswith('free'):
        A[-1, :] = 0
        A[-1, -3:] = [1, -2, 1]
        A[-2, :] = 0
        A[-2, -4:] = [-1, 3, -3, 1]
        efree = True

    # Only one inversion is needed for implicit spline energy minimization:
    inv = np.linalg.inv(A + gamma*np.eye(n))
    '''from_cpp = np.zeros(n*n).reshape(n,n)
    index=0
    xablau = inv #A+gamma*np.eye(n)
    
    np.savetxt("/tmp/pocoto",xablau)
        
    with open("/tmp/matrix_319") as fp:
        source = fp.readlines()
        for line in source:
            res = line.split()
            from_cpp[index,:]=np.array([float(x) for x in res])
            index+=1
    print("PRINTANDO ERRO")
    print(xablau - from_cpp)
    print("MAIS MAIOR")
    mais_feio = xablau - from_cpp
    print(np.max(mais_feio))
    result = np.where( mais_feio == np.amax(mais_feio))
    print(result)'''

    # Explicit time stepping for image energy minimization:
    for i in range(max_iterations):
        fx = intp(x, y, dx=1, grid=False)
        fy = intp(x, y, dy=1, grid=False)
        if sfixed:
            fx[0] = 0
            fy[0] = 0
        if efixed:
            fx[-1] = 0
            fy[-1] = 0
        if sfree:
            fx[0] *= 2
            fy[0] *= 2
        if efree:
            fx[-1] *= 2
            fy[-1] *= 2
        xn = inv @ (gamma*x + fx)
        yn = inv @ (gamma*y + fy)

        # Movements are capped to max_px_move per iteration:
        dx = max_px_move*np.tanh(xn-x)
        dy = max_px_move*np.tanh(yn-y)
        if sfixed:
            dx[0] = 0
            dy[0] = 0
        if efixed:
            dx[-1] = 0
            dy[-1] = 0
        x += dx
        y += dy

        # Convergence criteria needs to compare to a number of previous
        # configurations since oscillations can occur.
        j = i % (convergence_order+1)
        if j < convergence_order:
            xsave[j, :] = x
            ysave[j, :] = y
        else:
            dist = np.min(np.max(np.abs(xsave-x[None, :]) +
                                 np.abs(ysave-y[None, :]), 1))
            if dist < convergence:
                print("breaking at cycle i:"+ str(i)+" with dist:"+str(dist))
                break

    if coordinates == 'xy':
        return np.stack([x, y], axis=1)
    else:
        return np.stack([y, x], axis=1)
    
coordinates = np.array([[123., 335.],
       [123., 334.],
       [122., 333.],
       [122., 332.],
       [122., 331.],
       [122., 330.],
       [122., 329.],
       [121., 328.],
       [121., 327.],
       [120., 326.],
       [120., 325.],
       [120., 324.],
       [120., 323.],
       [119., 322.],
       [119., 321.],
       [119., 320.],
       [118., 319.],
       [118., 318.],
       [118., 317.],
       [118., 316.],
       [117., 315.],
       [117., 314.],
       [117., 313.],
       [117., 312.],
       [117., 311.],
       [117., 310.],
       [117., 309.],
       [117., 308.],
       [116., 307.],
       [116., 306.],
       [116., 305.],
       [116., 304.],
       [116., 303.],
       [116., 302.],
       [116., 301.],
       [116., 300.],
       [116., 299.],
       [116., 298.],
       [116., 297.],
       [116., 296.],
       [115., 295.],
       [115., 294.],
       [115., 293.],
       [115., 292.],
       [115., 291.],
       [115., 290.],
       [115., 289.],
       [115., 288.],
       [115., 287.],
       [115., 286.],
       [115., 285.],
       [115., 284.],
       [115., 283.],
       [115., 282.],
       [116., 281.],
       [116., 280.],
       [116., 279.],
       [116., 278.],
       [116., 277.],
       [116., 276.],
       [116., 275.],
       [117., 274.],
       [117., 273.],
       [117., 272.],
       [118., 271.],
       [118., 270.],
       [119., 269.],
       [119., 268.],
       [120., 267.],
       [121., 266.],
       [121., 265.],
       [122., 264.],
       [122., 263.],
       [123., 262.],
       [123., 261.],
       [123., 260.],
       [124., 259.],
       [124., 258.],
       [124., 257.],
       [125., 256.],
       [125., 255.],
       [125., 254.],
       [126., 253.],
       [126., 252.],
       [126., 251.],
       [126., 250.],
       [126., 249.],
       [126., 248.],
       [126., 247.],
       [126., 246.],
       [126., 245.],
       [126., 244.],
       [126., 243.],
       [127., 242.],
       [127., 241.],
       [127., 240.],
       [128., 239.],
       [128., 238.],
       [128., 237.],
       [128., 236.],
       [128., 235.],
       [128., 234.],
       [129., 233.],
       [129., 232.],
       [130., 231.],
       [130., 230.],
       [130., 229.],
       [130., 228.],
       [131., 227.],
       [131., 226.],
       [132., 225.],
       [132., 224.],
       [133., 223.],
       [133., 222.],
       [133., 221.],
       [133., 220.],
       [134., 219.],
       [134., 218.],
       [134., 217.],
       [134., 216.],
       [135., 215.],
       [136., 214.],
       [137., 213.],
       [137., 212.],
       [138., 211.],
       [139., 210.],
       [140., 209.],
       [140., 208.],
       [141., 207.],
       [141., 206.],
       [141., 205.],
       [142., 204.],
       [142., 203.],
       [143., 202.],
       [143., 201.],
       [144., 200.],
       [144., 199.],
       [145., 198.],
       [145., 197.],
       [145., 196.],
       [146., 195.],
       [146., 194.],
       [146., 193.],
       [146., 192.],
       [146., 191.],
       [146., 190.],
       [146., 189.],
       [146., 188.],
       [146., 187.],
       [146., 186.],
       [146., 185.],
       [146., 184.],
       [146., 183.],
       [147., 182.],
       [148., 181.],
       [149., 180.],
       [150., 179.],
       [151., 178.],
       [152., 177.],
       [153., 176.],
       [154., 175.],
       [155., 174.],
       [156., 173.],
       [157., 172.],
       [158., 171.],
       [159., 170.],
       [160., 169.],
       [161., 168.],
       [162., 167.],
       [163., 166.],
       [164., 165.],
       [165., 164.],
       [166., 163.],
       [167., 162.],
       [168., 161.],
       [169., 160.],
       [170., 159.],
       [171., 158.],
       [171., 157.],
       [172., 156.],
       [172., 155.],
       [173., 154.],
       [173., 153.],
       [174., 152.],
       [174., 151.],
       [175., 150.],
       [175., 149.],
       [176., 148.],
       [177., 147.],
       [178., 146.],
       [179., 145.],
       [180., 144.],
       [181., 143.],
       [182., 142.],
       [183., 141.],
       [184., 140.],
       [185., 139.],
       [186., 138.],
       [187., 138.],
       [188., 137.],
       [189., 136.],
       [190., 135.],
       [191., 135.],
       [192., 134.],
       [193., 134.],
       [194., 134.],
       [195., 133.],
       [196., 133.],
       [197., 133.],
       [198., 132.],
       [199., 131.],
       [200., 131.],
       [201., 130.],
       [202., 130.],
       [203., 130.],
       [204., 129.],
       [205., 129.],
       [206., 129.],
       [207., 128.],
       [208., 128.],
       [209., 127.],
       [210., 127.],
       [211., 127.],
       [212., 126.],
       [213., 126.],
       [214., 126.],
       [215., 125.],
       [216., 124.],
       [217., 123.],
       [218., 122.],
       [219., 122.],
       [220., 122.],
       [221., 121.],
       [222., 121.],
       [223., 120.],
       [224., 120.],
       [225., 119.],
       [226., 119.],
       [227., 118.],
       [228., 118.],
       [229., 118.],
       [230., 117.],
       [231., 116.],
       [232., 115.],
       [233., 114.],
       [234., 113.],
       [235., 112.],
       [235., 111.],
       [236., 110.],
       [237., 109.],
       [237., 108.],
       [237., 107.],
       [238., 106.],
       [238., 105.],
       [239., 104.],
       [239., 103.],
       [239., 102.],
       [240., 101.],
       [241., 100.],
       [241.,  99.],
       [241.,  98.],
       [242.,  97.],
       [242.,  96.],
       [242.,  95.],
       [242.,  94.],
       [243.,  93.],
       [243.,  92.],
       [244.,  91.],
       [244.,  90.],
       [244.,  89.],
       [245.,  88.],
       [245.,  87.],
       [245.,  86.],
       [245.,  85.],
       [246.,  84.],
       [246.,  83.],
       [246.,  82.],
       [247.,  81.],
       [247.,  80.],
       [248.,  79.],
       [248.,  78.],
       [248.,  77.],
       [248.,  76.],
       [249.,  75.],
       [249.,  74.],
       [250.,  73.],
       [250.,  72.],
       [250.,  71.],
       [250.,  70.],
       [251.,  69.],
       [251.,  68.],
       [251.,  67.],
       [252.,  66.],
       [252.,  65.],
       [252.,  64.],
       [253.,  63.],
       [253.,  62.],
       [253.,  61.],
       [254.,  60.],
       [254.,  59.],
       [254.,  58.],
       [254.,  57.],
       [254.,  56.],
       [255.,  55.],
       [255.,  54.],
       [255.,  53.],
       [255.,  52.],
       [256.,  51.],
       [256.,  50.],
       [256.,  49.],
       [257.,  48.],
       [257.,  47.],
       [257.,  46.],
       [257.,  45.],
       [258.,  44.],
       [258.,  43.],
       [258.,  42.],
       [258.,  41.],
       [257.,  40.]])


def split_coords():
    xxx=coordinates[:,0].copy(order="C").astype("int32")
    yyy=coordinates[:,1].copy(order="C").astype("int32")
    return xxx,yyy

for im in glob.glob("/home/tiago/workspace/angio/frames/*.png"):
    image = imageio.imread(im)
    images[os.path.basename(im)]=image

pw_line = -4.5
pw_edge = -1
flomage = images['46.png'].copy(order='C').astype('float32')
flomage = flomage/np.max(flomage)
img  = pw_line*flomage + pw_edge*sobel(flomage)

xxx=coordinates[:,0].copy(order="C").astype("int32")
yyy=coordinates[:,1].copy(order="C").astype("int32")
print("...")
from_cpp =active_cpp.active_cpp(img,xxx,yyy, 1.0, 1.0,0.1,0.1,0,0)
h1 = from_cpp[:len(from_cpp)//2]
h2 = from_cpp[len(from_cpp)//2:]
f= zip(h1,h2)
recomposed_coord = np.array([g for g in f])
print("...")
from_python = active_contour(images['46.png'], coordinates, bc='free',
                       alpha=1.0, beta=1.0, w_line=pw_line, w_edge=pw_edge, gamma=0.1)

print("MAX error:" + str(np.max(np.absolute(from_python - recomposed_coord))))
print("Average squared error:"+str( np.sum((from_python- recomposed_coord)**2)/len(from_python)))
#print("PYTHON:"+str(from_python))
#print("CPP:"+str(recomposed_coord))
