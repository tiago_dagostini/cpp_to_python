import cython

#distutils: language=c++
import numpy as np
cimport numpy as np

from libcpp.vector cimport vector


cdef extern from "c_active_cpp.cpp":
    vector[double] c_active_cpp(float * input, int sx, int sy, int *x_coords, int* y_coords, int coord_count, float a, float b, float g,float c, int sf, int ef);

@cython.boundscheck(False)
@cython.wraparound(False)

def active_cpp(np.ndarray[float,ndim=2, mode="c"] input not None , np.ndarray[int,ndim=1,mode="c"] x not None, np.ndarray[int,ndim=1,mode="c"] y not None, float alpha, float beta, float gamma,float conv, int sf, int ef):
    cdef int width,height
    cdef int c_count
    width = input.shape[1]
    height = input.shape[0]
    c_count = len(x)
    return c_active_cpp(&input[0,0],width,height,&x[0], &y[0], c_count, alpha, beta, gamma,conv, sf, ef)
