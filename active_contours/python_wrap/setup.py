#!/usr/bin/env python3

from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
import os
import numpy
import sys
import re
from distutils import sysconfig
from distutils.core import setup, Extension

if sys.platform == 'linux' or sys.platform == 'darwin':
  sysconfig.get_config_var(None)  # to fill up _config_vars
  d = sysconfig._config_vars
  
  print(d)
  for x in ['OPT', 'CFLAGS', 'PY_CFLAGS','PY_CPPFLAGS','PY_LDFLAGS', 'PY_CORE_CFLAGS', 'CONFIGURE_CFLAGS',  'LDSHARED']:
    d[x] = re.sub(' -g ', ' ', d[x])
    d[x] = re.sub('^-g ', '',  d[x])
    d[x] = re.sub(' -g$', '',  d[x])
    d[x] = re.sub(' -O2 ', ' -O3 ', d[x])
    d[x] = re.sub('^-O2 ', ' -O3 ',  d[x])
    d[x] = re.sub(' -O2$', ' -O3 ',  d[x])
    d[x] = re.sub(' -O1 ', ' -O3 ', d[x])
    d[x] = re.sub('^-O1 ', ' -O3 ',  d[x])
    d[x] = re.sub(' -O1$', ' -O3 ',  d[x])
	


os.environ["CC"] = "g++-8"
setup(
    cmdclass = {'build_ext': build_ext},
    ext_modules = [Extension("active_cpp",sources=["active_cpp.pyx"], language="c++",include_dirs=[numpy.get_include(),"/home/tiago/workspace/active_contours","/home/tiago/workspace/active_contours/cpputils/include/","/home/tiago/workspace/eigen/eigen-3.3.7/"])],
)
