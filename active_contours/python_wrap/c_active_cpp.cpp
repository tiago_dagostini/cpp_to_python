#include <active_contours.h>
#include <vector>

using namespace std;

extern "C" vector<double> c_active_cpp(float * input, int sx, int sy, int *x_coords, int* y_coords, int coord_count, float a, float b, float g,float c,  int sf, int ef)
{

        auto tmp = snakes::active_contour(input,sx, sy, x_coords, y_coords, coord_count, a, b, g, 2500, c,1.0f, sf==1, ef==1,false);
	std::vector<double> result;
	std::copy(begin(std::get<0>(tmp)),end(std::get<0>(tmp)),std::back_inserter(result));
	std::copy(begin(std::get<1>(tmp)),end(std::get<1>(tmp)),std::back_inserter(result));
	return result;

}

