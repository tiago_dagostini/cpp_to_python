#pragma once
#include <vector>
#include <array>
#include <math/p_array.h>
#include <cmath>
#include <iostream>
#include <fstream>
#include <Eigen/Dense>



namespace snakes{

    /*************Eigen helpers*********************/
    template<typename A, typename T>
    static inline void make_line_impl(A &v, T t)
    {
        v, t;
    }

    template<typename A, typename T, typename ...Ts>
    static inline void make_line_impl(A &v, T t, Ts... ts)
    {
        auto temp = (v, t);
        make_line_impl( temp, ts...);
    }


    template<typename T, typename ...Ts>
    Eigen::Matrix<T,1,-1> make_dyn_line(T t, Ts... ts)
    {
        Eigen::Matrix<T,1,-1> v( 1 + sizeof...(ts) );
        auto temp = v << t;
        make_line_impl(temp, ts...);
        return v;
    }

    template<typename T, typename ...Ts>
    auto make_line(T t, Ts... ts)->Eigen::Matrix<T,1, sizeof...(Ts)+1,1>
    {
        Eigen::Matrix<T,1, sizeof...(Ts)+1,1> v( 1 + sizeof...(ts) );
        auto temp = v << t;
        make_line_impl(temp, ts...);
        return v;
    }

    /*********Multi range operations********************************/
    template<typename T1, typename T2>
    struct range_pairs{

	T1 beg1;
	T2 beg2;
	T1 end1;
	T2 end2;

	struct pair_it{
            T1 c1;
            T2 c2;
            int index = 0;
            using RT1 = decltype(*c1);
            using RT2 = decltype(*c2);
            
            std::tuple<int,RT1,RT2> operator*()const {return {index,*c1,*c2};}
            pair_it operator++(){ c1++; c2++; index++; return *this;}
            bool operator==(const pair_it& other)const {return c1 == other.c1 && c2 == other.c2;} 
            bool operator!=(const pair_it& other)const {return c1 != other.c1 || c2 != other.c2;} 

	};

	pair_it begin(){return pair_it{beg1,beg2};}
	pair_it end(){return pair_it{end1,end2};}
	range_pairs(T1 b1, T1 e1, T2 b2, T2 e2) : beg1(b1), end1(e1), beg2(b2), end2(e2){}
    };

    template<typename T1, typename T2>
    range_pairs<T1,T2> indexed_pair_up(T1 b1, T1 e1, T2 b2, T2 e2){ return range_pairs<T1,T2>(b1,e1,b2,e2);}

    template<typename T1, typename T2, typename L>
    void execute_on_indexed_pairings(T1 b1, T1 e1, T2 b2, T2 e2,L functor)
    {
        for(const auto& p: indexed_pair_up(b1,e1,b2,e2))functor(std::get<0>(p),std::get<1>(p),std::get<2>(p));
    }
    
    template<typename T1, typename T2, typename T3>
    struct range_triplets{

	T1 beg1;
	T2 beg2;
        T3 beg3;
	T1 end1;
	T2 end2;
        T3 end3;

	struct triplet_it{
            T1 c1;
            T2 c2;
            T3 c3;
            int index = 0;
            using RT1 = decltype(*c1);
            using RT2 = decltype(*c2);
            using RT3 = decltype(*c3);
            
            std::tuple<int,RT1,RT2,RT3> operator*()const {return {index,*c1,*c2,*c3};}
            triplet_it operator++(){ c1++; c2++;c3++; index++; return *this;}
            bool operator==(const triplet_it& other)const {return c1 == other.c1 && c2 == other.c2 && c3 == other.c3;} 
            bool operator!=(const triplet_it& other)const {return c1 != other.c1 || c2 != other.c2 || c3 != other.c3;} 

	};

	triplet_it begin(){return triplet_it{beg1,beg2,beg3};}
        triplet_it end(){return triplet_it{end1,end2,end3};}
	range_triplets(T1 b1, T1 e1, T2 b2, T2 e2, T3 b3, T3 e3) : beg1(b1), end1(e1), beg2(b2), end2(e2),beg3(b3),end3(e3){}
    };
    
    template<typename T1, typename T2, typename T3>
    range_triplets<T1,T2,T3> indexed_triplet_pack(T1 b1, T1 e1, T2 b2, T2 e2, T3 b3, T3 e3){ return range_triplets<T1,T2,T3>(b1,e1,b2,e2,b3,e3);}

    template<typename T1, typename T2, typename T3, typename L>
    void execute_on_indexed_triplets(T1 b1, T1 e1, T2 b2, T2 e2,T3 b3, T3 e3, L functor)
    {
        for(const auto& p: indexed_triplet_pack(b1,e1,b2,e2,b3,e3))functor(std::get<0>(p),std::get<1>(p),std::get<2>(p),std::get<3>(p));
    }
    /////
    template<typename T1, typename T2,typename T3, typename T4>
    struct range_quads{

	T1 beg1;
	T2 beg2;
        T3 beg3;
        T4 beg4;
	T1 end1;
	T2 end2;
        T3 end3;
        T4 end4;

	struct quads_it{
            T1 c1;
            T2 c2;
            T3 c3;
            T4 c4;
            int index = 0;
            using RT1 = decltype(*c1);
            using RT2 = decltype(*c2);
            using RT3 = decltype(*c3);
            using RT4 = decltype(*c3);
            
            std::tuple<int,RT1,RT2,RT3,RT4> operator*()const {return {index,*c1,*c2,*c3,*c4};}
            quads_it operator++(){ c1++; c2++;c3++;c4++; index++; return *this;}
            bool operator==(const quads_it& other)const {return c1 == other.c1 && c2 == other.c2 && c3 == other.c3 && c4 == other.c4;} 
            bool operator!=(const quads_it& other)const {return c1 != other.c1 || c2 != other.c2 || c3 != other.c3 || c4 != other.c4;} 

	};

	quads_it begin(){return quads_it{beg1,beg2,beg3};}
        quads_it end(){return quads_it{end1,end2,end3};}
	range_quads(T1 b1, T1 e1, T2 b2, T2 e2, T3 b3, T3 e3,T4 b4, T4 e4) : beg1(b1), end1(e1),
                                                                                beg2(b2), end2(e2),
                                                                                beg3(b3), end3(e3),
                                                                                beg4(b4), end4(e4){}
    };
    
    template<typename T1, typename T2, typename T3,typename T4>
    range_quads<T1,T2,T3,T4> indexed_quads_pack(T1 b1, T1 e1, T2 b2, T2 e2, T3 b3, T3 e3, T4 b4, T4 e4){ return range_quads<T1,T2,T3,T4>(b1,e1,b2,e2,b3,e3,b4,e4);}

    template<typename T1, typename T2, typename T3, typename T4,typename L>
    void execute_on_indexed_quads(T1 b1, T1 e1, T2 b2, T2 e2,T3 b3, T3 e3, T4 b4, T4 e4, L functor)
    {
        for(const auto& p: indexed_quads_pack(b1,e1,b2,e2,b3,e3,b4,e4))functor(std::get<0>(p),std::get<1>(p),std::get<2>(p),std::get<3>(p),std::get<4>(p));
    }
    
    ///

    template<typename T1>
    struct range_single{

	T1 beg1;	
	T1 end1;


	struct single_it{
            T1 c1;

            int index = 0;
            using RT1 = decltype(*c1);
            
            std::tuple<int,RT1> operator*()const {return {index,*c1};}
            single_it operator++(){ c1++; index++; return *this;}
            bool operator==(const single_it& other)const {return c1 == other.c1 ;} 
            bool operator!=(const single_it& other)const {return c1 != other.c1 ;} 

	};

	single_it begin(){return single_it{beg1};}
	single_it end(){return single_it{end1};}
	range_single(T1 b1, T1 e1) : beg1(b1), end1(e1){}
    };

    template<typename T1>
    range_single<T1> indexed_singles(T1 b1, T1 e1){ return range_single<T1>(b1,e1);}

    template<typename T1, typename T2, typename L>
    void execute_on_indexed_range(T1 b1, T1 e1, L functor)
    {
        for(const auto& p: indexed_pair_up(b1,e1))functor(std::get<0>(p),std::get<1>(p));
    }

    struct i_range{

        int s;
        int e;

        struct i_r_it{
            int v;
            int operator*() const {return v;}
            i_r_it operator++(){v++; return *this;}
            bool operator==(const i_r_it& o) const {return v == o.v;}
            bool operator!=(const i_r_it& o) const {return v != o.v;}
        };

        i_r_it begin(){return i_r_it{s};}
        i_r_it end(){return i_r_it{e};}
        i_range(int b, int f):s(b),e(f){}
    };
    
    static inline i_range make_range(int b,int c) { return i_range{b,c};}

   

    /****************Active contours************************************/
    template<typename T>
    static inline void copy_vector_to_row(Eigen::MatrixXd &m, const std::vector<T>& b, int line)
    {
        auto l = m.row(line);
        for(auto i =0;i < b.size(); i++)
            l(i)=b[i];    
    }

    static inline Eigen::MatrixXd  build_beta_snake_matrix(int n)
    {
        Eigen::MatrixXd m(n,n);
        std::vector<double> v(n);
        std::fill(begin(v),end(v),0);
        v[0]=6;
        v[1]=-4;
        v[2]= 1;
        v[n-1]=-4;
        v[n-2]=1;
        for(auto row=0; row < n; row++)
        {
            auto nr = v;
       
            std::rotate(begin(nr),end(nr)-row,end(nr));
            auto lr = m.row(row);
            for(int i =0; i <n; i++)
            {
                lr(i) = nr[i];
            }
        }
        return m;
    }

    static inline Eigen::MatrixXd  build_alpha_snake_matrix(int n)
    {
        Eigen::MatrixXd m(n,n);
        std::vector<double> v(n);
        std::fill(begin(v),end(v),0);
        v[0]=-2;
        v[1]=1;
        v[n-1]=1;
        //this is the standard line vector and we go on from this point
        for(auto row=0; row < n; row++)
        {
            auto nr = v;
       
            std::rotate(begin(nr),end(nr)-row,end(nr));
            auto lr = m.row(row);
            for(int i =0; i <n; i++)
            {
                lr(i) = nr[i];
            }
        }
        return m;
    }


    namespace Bicubic
    {
    
        template<typename T>
        using FourPixels = T*;


        template<typename T>
        inline double cubicInterpolate(const FourPixels<T>& p, double t)
        {
            using namespace pixeon::math;       

            double res  = p[1] +
                (
                 (p[2] - p[0] +
                  (
                   (2*p[0] - 5*p[1] + 4*p[2] - p[3]) + (3*(p[1] - p[2]) + p[3] - p[0])*t
                   )*t
                  )*t
                 )/2;

            return res;
        }


        template<typename T>
        inline const FourPixels<T> make_quad_vec(const T* input,
                                                 std::int32_t input_width,
                                                 int x,
                                                 int y)
        {

            x = std::max(1,x);
            x = std::min(input_width -3,x);
            return  {const_cast<T*>(input + y*input_width +x -1)};
       
        }

        template<typename T>
        inline double interpolate(const T*  input,
                                 int input_width,
                                 int input_height,
                                 pixeon::math::vector<double,2> const &position)
                            
        {
            const double sp = position[0];
            const double tp = position[1];

            const int x1 = sp;

            const int y1 = tp;
            const int y0 = y1 == 0 ? y1 : y1 -1;
            const int y2 = y1 +1 >= input_height ? y1 : y1 +1;
            const int y3 = y1 +2 >= input_height ? y2 : y1 +2;
              
            const auto q0 = make_quad_vec(input, input_width, x1, y0);
            const auto q1 = make_quad_vec(input, input_width, x1, y1);
            const auto q2 = make_quad_vec(input, input_width, x1, y2);
            const auto q3 = make_quad_vec(input, input_width, x1, y3);

            const std::array<double,4> arr =
                {
                    cubicInterpolate<T>(q0, sp - x1),
                    cubicInterpolate<T>(q1, sp - x1),
                    cubicInterpolate<T>(q2, sp - x1),
                    cubicInterpolate<T>(q3, sp - x1),
                };

            auto tmp = cubicInterpolate(arr.data(), tp - y1);        

            return {tmp};
        }
    };

    template<typename T>
    std::vector<double> dx(const T* image, int image_width, int image_height)
    {
        std::vector<double> result(image_width * image_height);
        std::fill(begin(result),end(result),0);
        for(int y =0 ; y < image_height; y++)
        {
            for(int x =1; x < image_width-1; x++)
            {
                auto pre = /*Bicubic::interpolate(image,image_width,image_height,{double(x)-0.5,double(y)});//*/image[x-1 + y*image_width];
                auto pos = /*Bicubic::interpolate(image,image_width,image_height,{double(x)+0.5,double(y)});//*/image[x+1 + y*image_width];
                result[x +y*image_width] = (pos-pre)/2.0;
           
            }
        
        }
        return result;
    }

    template<typename T>
    std::vector<double> dy(const T* image, int image_width, int image_height)
    {
        std::vector<double> result(image_width * image_height);
        std::fill(begin(result),end(result),0);
        for(int y=1 ; y < image_height-1; y++)
        {
            for(int x =0; x < image_width; x++)
            {
                auto pre = /*Bicubic::interpolate(image,image_width,image_height,{double(x),y-0.5}); //*/image[x + (y-1)*image_width];
                auto pos = /*Bicubic::interpolate(image,image_width,image_height,{double(x),y+0.5}); //*/image[x + (y+1)*image_width];
                result[x +y*image_width] = (pos - pre)/2.0;
            }
        
        }
        return result;
    }

   
    
    template<typename T>
    std::tuple<std::vector<double>,std::vector<double> >  active_contour(const T* image,
                                                                       int image_width,
                                                                       int image_height,
                                                                       int* x_coords,
                                                                       int* y_coords,
                                                                       int coord_count,
                                                                       float alpha,
                                                                       float beta,
                                                                       float gamma,                                                     
                                                                       int max_iterations=2500,
                                                                       float convergence =0.1f,
                                                                       float max_px_move = 1.0f,
                                                                       bool start_fixed = false,
                                                                       bool end_fixed = false,
                                                                       bool periodic = false )
    {
       
        //This code expects coordinates in xy... NOT row columns
        //If periodic  is True then start and end fixed are simply worthless


        int convergence_order = 20;
        std::tuple<std::vector<double>, std::vector<double> > result = {std::vector<double>(x_coords, x_coords + coord_count),
                                                                        std::vector<double>(y_coords, y_coords + coord_count)};
        //std::vector<float> x(x_coords, x_coords + coord_count);
        //std::vector<float> y(y_coords, y_coords + coord_count);
        std::vector<double>& x = std::get<0>(result);
        std::vector<double>& y = std::get<1>(result);


        //the attraction to border or to  line is handled by supplying  an image as such that w_line*img + w_edge*edge
        //I advise the usage of float in the image type
        auto dx_map = dx(image, image_width, image_height);
        auto dy_map = dy(image, image_width, image_height);

        auto a_matrix = build_alpha_snake_matrix(coord_count);
        auto b_matrix = build_beta_snake_matrix(coord_count);

        //now we deal with the endpoint behaviors
       
        Eigen::MatrixXd  composite_A = (a_matrix*(-alpha)) + (b_matrix*beta);

        auto sfixed = false;
        auto efixed = false;
        auto sfree = false;
        auto efree = false;
        auto n = coord_count;
        std::vector<std::vector<double> > xsave(convergence_order,std::vector<double>(n));
        std::vector<std::vector<double> > ysave(convergence_order,std::vector<double>(n));
        std::vector<std::vector<double> > xsavediff(convergence_order,std::vector<double>(n));
        std::vector<std::vector<double> > ysavediff(convergence_order,std::vector<double>(n));
        std::vector<float> max_local_conv(n);
       
        Eigen::Matrix<double,1,-1> zero_line = Eigen::Matrix<double,1,-1>::Zero(n);

        
        if(periodic == false)
        {
            if(start_fixed)
            {
               
                sfixed = true;
                composite_A.block(0,0,1,coord_count) = zero_line;
                composite_A.block(1,0,1,coord_count) = zero_line;
                composite_A.block(1,0,1,3)= make_line(1.0,-2.0,1.0);
            }
            else //free
            {
               
                sfree = true;                        
                composite_A.block(0,0,1,n) = zero_line;
               
                composite_A.block(0,0,1,3)=make_line(1.0,-2.0,1.0);
               
                composite_A.block(1,0,1,n) = zero_line;
               
                composite_A.block(1,0,1,4)=make_line(-1.0,3.0,-3.0,1.0);
               
            }
            if(end_fixed)
            {
               
                efixed = true;
                composite_A.block(n-1,0,1,coord_count) = zero_line;
                composite_A.block(n-2,0,1,coord_count) = zero_line;
                composite_A.block(n-2,n-3,1,3)=make_line(1.0,-2.0,1.0);
            }
            else
            {
               
                efree = true;
      
                composite_A.block(n-1,0,1,n) = zero_line;
      
                composite_A.block(n-1,n-3,1,3)= make_line(1.0,-2.0,1.0);
      
                composite_A.block(n-2,0,1,n) = zero_line;
      
                composite_A.block(n-2,n-4,1,4)= make_line(-1.0,3.0,-3.0,1.0);
            }
        }
        
        Eigen::MatrixXd inv = (composite_A + (Eigen::MatrixXd::Identity(n,n)*gamma));
        inv = inv.inverse();
        // {
        //      Eigen::MatrixXd bola(3,3);
        //      bola<<3,4,5,6,7,8,1,2,3; // = composite_A + Eigen::MatrixXd::Identity(n,n)*gamma;
        //     std::stringstream s;
        //     s<<"/tmp/matrix_";
        //     s<<n;
        //     std::ofstream filo(s.str().c_str());
        //     std::stringstream ss;
        //     ss << inv;
        //     filo<<ss.str();                       
        // }
 
        std::vector<double> fx(x.size());
        std::vector<double> fy(x.size()); //both sizes shoudl be same

       
        std::vector<double> x_corrections(n);
        std::vector<double> y_coorections(n);
        Eigen::VectorXd gxf(n,1);
        Eigen::VectorXd gyf(n,1);
        Eigen::VectorXd xn(n,1);
        Eigen::VectorXd yn(n,1);
       
        for( auto iteration : make_range(0,max_iterations))
        
        {

            using namespace std;
                               
            execute_on_indexed_pairings(begin(x),end(x),begin(y),end(y),[&](int i, double a, double b)
                                        {
                                            fx[i] = Bicubic::interpolate(dx_map.data(),image_width,image_height,{a,b});
                                            fy[i] = Bicubic::interpolate(dy_map.data(),image_width,image_height,{a,b});
                                        });

             
            if(sfixed)  { fx[0]=0;     fy[0]=0; }
            if(efixed)  { fx.back()=0; fy.back()=0;}
            if(sfree)   { fx[0] *=2;   fy[0] *=2;}
            if(efree)   { fx.back() *=2; fy.back() *=2;}

            execute_on_indexed_pairings(begin(x),end(x),begin(fx),end(fx),[&gxf,gamma](int i, double a, double b){gxf[i]=gamma*a+b;});
            execute_on_indexed_pairings(begin(y),end(y),begin(fy),end(fy),[&gyf,gamma](int i, double a, double b){gyf[i]=gamma*a+b;});

            xn.noalias() = inv*gxf;
            yn.noalias() = inv*gyf;

            int is =0;
            int ie =n;
            if(sfixed)
                is+=1;
            if(efixed)
                ie-=1;
            for(int i = is; i < ie; i++)
            {
                x[i] += max_px_move*std::tanh(xn[i] - x[i]);
                y[i] += max_px_move*std::tanh(yn[i] - y[i]);
                //x[i] = std::max(0.0,std::min(x[i],double(image_width)));
                //y[i] = std::max(0.0,std::min(y[i],double(image_height)));
            }

            auto j = iteration % (convergence_order+1);
            if( j < convergence_order)
            {
                
                for(auto k =0; k< n; k++)
                {
                    xsave[j][k]=x[k];
                    ysave[j][k]=y[k];
                }
    
            }
            else
            {
                
                auto dist =9999999990.0f;
                for(auto i =0 ; i < xsave.size(); i++)
                {                
                    {
                    auto& target = xsavediff[i];
                    auto& source = xsave[i];
                    execute_on_indexed_pairings(begin(source),end(source),begin(x),end(x),[&target](int index, double a, double b)
                                                {
                                                    target[index] = std::abs(a - b);
                                                });
                    }
                    {
                    auto& target = ysavediff[i];
                    auto& source = ysave[i];
                    execute_on_indexed_pairings(begin(source),end(source),begin(y),end(y),[&target](int index, double a, double b)
                                                {
                                                    target[index] = std::abs(a - b);
                                                });
                    }
                    auto line_max =0.0f;
                    execute_on_indexed_pairings(begin(xsavediff[i]),end(xsavediff[i]),begin(ysavediff[i]),end(ysavediff[i]),
                                                [&line_max](int i, double a, double b)
                                                {
                                                    if( (a +b) > line_max) line_max = a+b;                                                    
                                                });
                    if(line_max <  dist)
                        dist = line_max;
                }

                if (dist < convergence)
                    {
                        std::cout<<"Conv:"<<dist<<" at cycle:"<<iteration<<std::endl;
                        break;
                    }
                
                /*
                  dist = np.min(np.max(np.abs(xsave-x[None, :]) +
                  np.abs(ysave-y[None, :]), 1))
                  if dist < convergence:
                  break
                */
                
            }

        }//for iterations
       
        return result;
    }//function

}//end of namespace
